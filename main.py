from genericpath import isfile
from posix import listdir
from mawiparse.utils import read_pcap_to_file
from os import listdir
from os.path import isfile, join
import os
import subprocess
import glob
from datetime import datetime

import time

def automated_processing():
    mypath = 'data/'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    onlyfiles.sort()

    
 
    for pcap_file in onlyfiles:
        start_time = time.time()

        files = glob.glob('data/out/*')
        for f in files:
            os.remove(f)

        if (".gz" in str(pcap_file)):
            print(f"{pcap_file} needs to be extracted.")
            continue
        print(f">>> Processing pcap file: {mypath + pcap_file}")
        split_cmd = "tcpdump -r "+ mypath+pcap_file +" -w " + mypath + "out/output" + " -C 250"
        print("Spliting pcap:" + split_cmd)
        os.system(split_cmd)        # split the pcap into 250MB chunks
        print(">> File splited.")

        os.system("mkdir " + "data/" + pcap_file.split('.')[0])
        print("parsing the pcap:")
        print("--- %s seconds ---\n" % (time.time() - start_time))
        read_pcap_to_file("data/out/", "data/" + pcap_file.split('.')[0] + '/' )
        print("Finished one pcap. --- %s seconds ---\n" % (time.time() - start_time))

if __name__ == '__main__':
    automated_processing()



    # naive() spent: Total of 262.8574552536011 seconds.


