from os import listdir
from os.path import isfile, join

import dpkt
from datetime import datetime
import multiprocessing
from collections import defaultdict
from dpkt.tcp import TCP
from dpkt.ip import IP, IP_PROTO_UDP
from dpkt.udp import UDP
from collections import Counter
import pickle



def read_pcap_to_file(input_dir, output_dir):
    """[summary]

    Args:
        input_dir (directory name): this contains the splitted pcap files directory that will be used for further analysing. 
        output_dir (directory name): this contains the output directory name for output pkl files. e.g., data/out/
    """
    
    vol_data, type_data, tcp_d, udp_d = directory_read_pcap(input_dir)
    # udp_s_file = open(output_dir + "udp_s.pkl", "wb")
    vol_data_file = open(output_dir + "vol_data.pkl", "wb")
    type_data_file = open(output_dir + "type_data.pkl", "wb")
    tcp_d_file = open(output_dir + "tcp_d.pkl", "wb")
    # tcp_s_file = open(output_dir + "tcp_s.pkl", "wb")
    udp_d_file = open(output_dir + "udp_d.pkl", "wb")
    
    pickle.dump(dict(vol_data), vol_data_file)
    pickle.dump(dict(type_data), type_data_file)
    pickle.dump(dict(tcp_d), tcp_d_file)
    # pickle.dump(dict(tcp_s), tcp_s_file)
    # pickle.dump(dict(udp_s), udp_s_file)
    pickle.dump(dict(udp_d), udp_d_file)
    
    vol_data_file.close()
    type_data_file.close()
    # tcp_s_file.close()
    tcp_d_file.close()
    udp_d_file.close()
    # udp_s_file.close()


def directory_read_pcap(mypath):
    # mypath = 'data/2020-03-01/out/'
    print(mypath)
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    onlyfiles.sort()

    # global dict init here
    manager = multiprocessing.Manager()

    final_pkt_volume_dict = manager.dict()
    final_pkt_type_dict = manager.dict()
    final_tcp_dport_dict = manager.dict()
    final_tcp_sport_dict = manager.dict()
    final_udp_dport_dict = manager.dict()
    final_udp_sport_dict = manager.dict()

    def worker(filepath):
        # the single thread that read the single file; this function should work concurrently
        # since there are 0-15 min of data for each pcap file per day
        # temp_volume_list = [0] * 16  # local volume list
        temp_vol_dict = defaultdict(int)
        temp_type_dict = defaultdict(int)

        temp_tcp_sport_dict = defaultdict(int)
        temp_tcp_dport_dict = defaultdict(int)
        temp_udp_sport_dict = defaultdict(int)
        temp_udp_dport_dict = defaultdict(int)

        try:
            f = open(filepath, 'rb')
        except:
            print("File Load Error.")
        pcap = dpkt.pcap.Reader(f)
        # pcap file read.
        for ts, buf in pcap:
            # iterate through all pkts
            # >> 1. check volume
            dt = datetime.fromtimestamp(ts)
            temp_vol_dict[int(dt.minute * 60 + dt.second)] += 1
            # temp_volume_list[int(dt.minute)] += 1

            # >> 2. check packet type
            eth = dpkt.ethernet.Ethernet(buf)
            ip = eth.data
            try:
                pkt_type = type(ip.data)
                temp_type_dict[pkt_type] += 1
            except:
                continue

            # >> 3. run port analysis: (TCP)
            if pkt_type == TCP:
                temp_tcp_dport_dict[ip.data.dport] += 1
                # temp_tcp_sport_dict[ip.data.sport] += 1

            # >> 4. run port analysis  (UDP)
            elif pkt_type == UDP:
                temp_udp_dport_dict[ip.data.dport] += 1
                # temp_udp_sport_dict[ip.data.sport] += 1

        # after iteration done, put into global variables
        # dummy coding...
        # for minute in range(16):
        #     if minute in final_pkt_volume_dict:
        #         final_pkt_volume_dict[minute] += temp_volume_list[minute]
        #     else:
        #         final_pkt_volume_dict[minute] = temp_volume_list[minute]
        for key in temp_vol_dict:
            if key not in final_pkt_volume_dict:
                final_pkt_volume_dict[key] = temp_vol_dict[key]
            else:
                final_pkt_volume_dict[key] += temp_vol_dict[key]

        for key in temp_type_dict:
            if key not in final_pkt_type_dict:
                final_pkt_type_dict[key] = temp_type_dict[key]
            else:
                final_pkt_type_dict[key] += temp_type_dict[key]

        # for key in temp_udp_sport_dict:
        #     if key not in final_udp_sport_dict:
        #         final_udp_sport_dict[key] = temp_udp_sport_dict[key]
        #     else:
        #         final_udp_sport_dict[key] += temp_udp_sport_dict[key]

        for key in temp_udp_dport_dict:
            if key not in final_udp_sport_dict:
                final_udp_dport_dict[key] = temp_udp_dport_dict[key]
            else:
                final_udp_dport_dict[key] += temp_udp_dport_dict[key]

        for key in temp_tcp_dport_dict:
            if key not in final_tcp_dport_dict:
                final_tcp_dport_dict[key] = temp_tcp_dport_dict[key]
            else:
                final_tcp_dport_dict[key] += temp_tcp_dport_dict[key]

        # for key in temp_tcp_sport_dict:
        #     if key not in final_tcp_sport_dict:
        #         final_tcp_sport_dict[key] = temp_tcp_sport_dict[key]
        #     else:
        #         final_tcp_sport_dict[key] += temp_tcp_sport_dict[key]

        print(f">> {filepath} finished.")

    # use multiprocessing
    processes = []
    for i in onlyfiles:
        p = multiprocessing.Process(target=worker, args=(mypath + i,))
        processes.append(p)
        p.start()

    for process in processes:
        process.join()
    print(final_pkt_volume_dict)
    print(final_pkt_type_dict)
    return final_pkt_volume_dict, final_pkt_type_dict, final_tcp_dport_dict, final_udp_dport_dict



def read_all_pkl(directory_name):
    vol_data = pickle.load(open(directory_name + 'vol_data.pkl', 'rb'))
    type_data = pickle.load(open(directory_name + 'type_data.pkl', 'rb'))
    tcp_d = pickle.load(open(directory_name + 'tcp_d.pkl', 'rb'))
    udp_d = pickle.load(open(directory_name + 'udp_d.pkl', 'rb'))
    
    return vol_data, type_data, tcp_d, udp_d
        